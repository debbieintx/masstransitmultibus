﻿using MassTransitMultiBus.Tests.Tests;

namespace MassTransitMultiBus.Tests.CustomConsumerId
{
    public class AllMessagesCustomConsumerIdMessage : ITestDataMessage
    {
        public string CorrelationId { get; set; }
        public string MessageRequestorId { get; set; }
        public string Data { get; set; }
    }
}