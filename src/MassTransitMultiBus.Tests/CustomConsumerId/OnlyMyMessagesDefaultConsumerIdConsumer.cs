﻿using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using MassTransitMultiBus.Tests.Tests;

namespace MassTransitMultiBus.Tests.CustomConsumerId
{
    public class OnlyMyMessagesDefaultConsumerIdConsumer : IConsumer<OnlyMyMessagesDefaultConsumerIdMessage>
    {
        private readonly Validator<ICustomConsumerIdBus> _validator;

        public OnlyMyMessagesDefaultConsumerIdConsumer(Validator<ICustomConsumerIdBus> validator)
        {
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<OnlyMyMessagesDefaultConsumerIdMessage> context)
        {
            // Consume the message so that it doesn't stay in the queue if the test fails
            await context.ConsumeCompleted;

            _validator.ValidateMessage(
                context.CorrelationId?.ToString("N"),
                context.Message,
                this.GetType().FullName,
                context.ReceiveContext?.InputAddress?.Segments.LastOrDefault());
        }
    }
}