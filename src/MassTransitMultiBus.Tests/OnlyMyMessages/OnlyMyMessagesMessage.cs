﻿using MassTransitMultiBus.Tests.Tests;

namespace MassTransitMultiBus.Tests.OnlyMyMessages
{
    public class OnlyMyMessagesMessage : ITestDataMessage
    {
        public string CorrelationId { get; set; }
        public string MessageRequestorId { get; set; }
        public string Data { get; set; }
    }
}