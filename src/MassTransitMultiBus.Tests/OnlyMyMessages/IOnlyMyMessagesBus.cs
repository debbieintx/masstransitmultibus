﻿using MassTransit;

namespace MassTransitMultiBus.Tests.OnlyMyMessages
{
    public interface IOnlyMyMessagesBus : IBus
    {

    }
}