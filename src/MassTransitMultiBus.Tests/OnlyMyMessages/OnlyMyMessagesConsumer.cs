﻿using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using MassTransitMultiBus.Tests.Tests;

namespace MassTransitMultiBus.Tests.OnlyMyMessages
{
    public class OnlyMyMessagesConsumer : IConsumer<OnlyMyMessagesMessage>
    {
        private readonly Validator<IOnlyMyMessagesBus> _validator;

        public OnlyMyMessagesConsumer(Validator<IOnlyMyMessagesBus> validator)
        {
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<OnlyMyMessagesMessage> context)
        {
            // Consume the message so that it doesn't stay in the queue if the test fails
            await context.ConsumeCompleted;

            _validator.ValidateMessage(
                context.CorrelationId?.ToString("N"),
                context.Message,
                this.GetType().FullName,
                context.ReceiveContext?.InputAddress?.Segments.LastOrDefault());
        }
    }
}