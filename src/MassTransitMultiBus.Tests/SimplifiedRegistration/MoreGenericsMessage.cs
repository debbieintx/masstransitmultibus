﻿using MassTransitMultiBus.Tests.Tests;

namespace MassTransitMultiBus.Tests.SimplifiedRegistration
{
    public class MoreGenericsMessage : ITestDataMessage
    {
        public string CorrelationId { get; set; }
        public string MessageRequestorId { get; set; }
        public string Data { get; set; }
    }

    public class MoreGenericsMessage<TThing1, TThing2> : ITestDataMessage
    where TThing1 : IThing1
    where TThing2 : IThing2
    {
        public TThing1 Thing1 { get; set; }
        public TThing2 Thing2 { get; set; }
        public string CorrelationId { get; set; }
        public string MessageRequestorId { get; set; }
        public string Data { get; set; }
    }
}