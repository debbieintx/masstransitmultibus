﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using MassTransit;
using MassTransitMultiBus.Messaging.Configurations;
using MassTransitMultiBus.Messaging.ConsumerDefinitions;
using MassTransitMultiBus.Messaging.Models;

namespace MassTransitMultiBus.Tests.SimplifiedRegistration
{
    public class MoreGenericsConsumerDefinition<TBus, TConsumer, TMessage, TSomething> : BaseConsumerDefinition<TBus, TConsumer, TMessage>
        where TBus : IBus
        where TConsumer : class, IConsumer<TMessage>
        where TMessage : class
        where TSomething : class
    {
        // Set messageRequestorFilter to null so base class will not filter messages
        public MoreGenericsConsumerDefinition(
            IMessagingConfiguration<TBus> messageBusConfiguration,
            IEnumerable<IMessageConsumerRegistration<TBus>> consumerRegistrations)
            : base(
                messageBusConfiguration,
                consumerRegistrations,
                typeof(MoreGenericsConsumerDefinition<TBus, TConsumer, TMessage, TSomething>))
        {
            // just do something showing a consumer definition can have more generic parameters
            var something = Activator.CreateInstance<TSomething>();
            something.GetType().Should().Be(typeof(TSomething));
        }
    }
}