﻿using MassTransitMultiBus.Tests.Tests;

namespace MassTransitMultiBus.Tests.SimplifiedRegistration
{
    public class SimplifiedRegistrationMessage : ITestDataMessage
    {
        public string CorrelationId { get; set; }
        public string MessageRequestorId { get; set; }
        public string Data { get; set; }
    }
}