﻿using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using MassTransitMultiBus.Tests.Tests;

namespace MassTransitMultiBus.Tests.SimplifiedRegistration
{
    public class MoreGenericsConsumer : IConsumer<MoreGenericsMessage>
    {
        private readonly Validator<ISimplifiedRegistrationBus> _validator;

        public MoreGenericsConsumer(Validator<ISimplifiedRegistrationBus> validator)
        {
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<MoreGenericsMessage> context)
        {
            // Consume the message so that it doesn't stay in the queue if the test fails
            await context.ConsumeCompleted;

            // ValidateMessage
            _validator.ValidateMessage(
                context.CorrelationId?.ToString("N"),
                context.Message,
                this.GetType().FullName,
                context.ReceiveContext?.InputAddress?.Segments.LastOrDefault());
        }
    }

    public class MoreGenericsConsumer<TMessage> : IConsumer<TMessage>
        where TMessage : class, ITestDataMessage
    {
        private readonly Validator<ISimplifiedRegistrationBus> _validator;

        public MoreGenericsConsumer(Validator<ISimplifiedRegistrationBus> validator)
        {
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<TMessage> context)
        {
            // Consume the message so that it doesn't stay in the queue if the test fails
            await context.ConsumeCompleted;

            // ValidateMessage
            _validator.ValidateMessage(
                context.CorrelationId?.ToString("N"),
                context.Message,
                this.GetType().FullName,
                context.ReceiveContext?.InputAddress?.Segments.LastOrDefault());
        }
    }
}