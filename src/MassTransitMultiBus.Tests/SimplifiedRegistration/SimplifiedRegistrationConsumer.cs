﻿using System.Linq;
using System.Threading.Tasks;

using MassTransit;

using MassTransitMultiBus.Tests.Tests;

namespace MassTransitMultiBus.Tests.SimplifiedRegistration
{
    public class SimplifiedRegistrationConsumer : IConsumer<SimplifiedRegistrationMessage>
    {
        private readonly Validator<ISimplifiedRegistrationBus> _validator;

        public SimplifiedRegistrationConsumer(Validator<ISimplifiedRegistrationBus> validator)
        {
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<SimplifiedRegistrationMessage> context)
        {
            // Consume the message so that it doesn't stay in the queue if the test fails
            await context.ConsumeCompleted;

            // ValidateMessage
            _validator.ValidateMessage(
                context.CorrelationId?.ToString("N"),
                context.Message,
                this.GetType().FullName,
                context.ReceiveContext?.InputAddress?.Segments.LastOrDefault());
        }
    }
}