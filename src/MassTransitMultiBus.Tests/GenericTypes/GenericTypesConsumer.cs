﻿using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using MassTransitMultiBus.Tests.Tests;

namespace MassTransitMultiBus.Tests.GenericTypes
{
    public class GenericTypesConsumer<TMessage> : IConsumer<TMessage>
    where TMessage : class, ITestDataMessage
    {
        private readonly Validator<IGenericTypesBus> _validator;

        public GenericTypesConsumer(Validator<IGenericTypesBus> validator)
        {
            _validator = validator;
        }

        public async Task Consume(ConsumeContext<TMessage> context)
        {
            // Consume the message so that it doesn't stay in the queue if the test fails
            await context.ConsumeCompleted;

            _validator.ValidateMessage(
                context.CorrelationId?.ToString("N"),
                context.Message,
                this.GetType().FullName,
                context.ReceiveContext?.InputAddress?.Segments.LastOrDefault());
        }
    }
}