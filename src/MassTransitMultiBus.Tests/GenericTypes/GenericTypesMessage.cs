﻿namespace MassTransitMultiBus.Tests.GenericTypes
{
    public class GenericTypesMessage : IGenericTypesMessage
    {
        public string CorrelationId { get; set; }
        public string MessageRequestorId { get; set; }
        public string GenericTypesData { get; set; }
        public string Data { get; set; }
    }

    public class GenericTypesMessage<TLevel1> : IGenericTypesMessage
    {
        public string CorrelationId { get; set; }
        public string MessageRequestorId { get; set; }
        public string Data { get; set; }
    }

    public class GenericTypesMessage<TLevel1, TLevel2> : IGenericTypesMessage
    {
        public string CorrelationId { get; set; }
        public string MessageRequestorId { get; set; }
        public string Data { get; set; }
    }
}