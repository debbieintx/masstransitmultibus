﻿namespace MassTransitMultiBus.Tests.Tests
{
    public class Expected
    {
        public ITestDataMessage Message { get; set; }
        public string ConsumerName { get; set; }
        public bool Received { get; set; } = false;
        public bool ShouldNotReceive { get; set; } = false; // for testing that messages get filtered out for OnlyMyMessages
        public string CustomConsumerId { get; set; } // for testing setting the ConsumerId part of the EndpointName
    }
}