﻿using MassTransitMultiBus.Abstractions.Interfaces;

namespace MassTransitMultiBus.Tests.Tests
{
    public interface ITestDataMessage : IMessageBase
    {
        string Data { get; set; }
    }
}