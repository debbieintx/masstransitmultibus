using System.Threading.Tasks;
using MassTransitMultiBus.Tests.OnlyMyMessages;
using Xunit;

namespace MassTransitMultiBus.Tests.Tests
{
    public class OnlyMyMessagesTests
    {
        private readonly Validator<IOnlyMyMessagesBus> _validator;
        public static readonly string AlternateAppId = "AlternateAppId";

        public OnlyMyMessagesTests(Validator<IOnlyMyMessagesBus> validator)
        {
            _validator = validator;
        }

        [Fact]
        public async Task CanConsumeAllMessagesMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<AllMessagesMessage>(
                typeof(AllMessagesConsumer).FullName);
            _validator.PrepareMessage<AllMessagesMessage>(
                typeof(AllMessagesConsumer).FullName, null, AlternateAppId);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task OnlyConsumeMyMessagesMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<OnlyMyMessagesMessage>(
                typeof(OnlyMyMessagesConsumer).FullName);
            _validator.PrepareMessage<OnlyMyMessagesMessage>(
                typeof(OnlyMyMessagesConsumer).FullName, null, AlternateAppId, shouldNotReceive: true);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeMultipleOfEachTypeOfTestMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            for (var i = 0; i < 10; i++)
            {
                _validator.PrepareMessage<AllMessagesMessage>(
                    typeof(AllMessagesConsumer).FullName);
                _validator.PrepareMessage<AllMessagesMessage>(
                    typeof(AllMessagesConsumer).FullName, null, AlternateAppId);
                _validator.PrepareMessage<OnlyMyMessagesMessage>(
                    typeof(OnlyMyMessagesConsumer).FullName);
                _validator.PrepareMessage<OnlyMyMessagesMessage>(
                    typeof(OnlyMyMessagesConsumer).FullName, null, AlternateAppId, shouldNotReceive: true);
            }

            await _validator.ActAndAssert();
        }
    }
}
