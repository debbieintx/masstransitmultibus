﻿using System.Reflection;
using MassTransitMultiBus.Abstractions.Models;
using MassTransitMultiBus.Messaging.Configurations;
using MassTransitMultiBus.Messaging.ConsumerDefinitions;
using MassTransitMultiBus.Messaging.Extensions;
using MassTransitMultiBus.Tests.CustomConsumerId;
using MassTransitMultiBus.Tests.GenericTypes;
using MassTransitMultiBus.Tests.OnlyMyMessages;
using MassTransitMultiBus.Tests.SimplifiedRegistration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MassTransitMultiBus.Tests.Tests
{
    public class Startup
    {
        // For testing, use strings without special characters since transport might change them.
        // ReSharper disable twice StringLiteralTypo
        public const string AllMessagesCustomConsumerId = "oooooAllMessagesCustomConsumerIdooooo";
        public const string OnlyMyMessagesCustomConsumerId = "oooooOnlyMyMessagesCustomConsumerIdooooo";

        public void ConfigureHost(IHostBuilder hostBuilder) =>
           hostBuilder
               .ConfigureHostConfiguration(builder => { })
               .ConfigureAppConfiguration((hostingContext, config) =>
               {
                   var env = hostingContext.HostingEnvironment;
                   env.EnvironmentName = "Development"; // This is a developer tool, so environment is always Development

                   config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);

                   if (env.IsDevelopment())
                   {
                       var appAssembly = Assembly.Load(new AssemblyName(env.ApplicationName));
                       config.AddUserSecrets(appAssembly, optional: true);
                   }

                   config.AddEnvironmentVariables();

                   // Xunit tests don't support command line arguments
                   //if (args != null) { config.AddCommandLine(args); }
               })
               .ConfigureServices((hostingContext, services) =>
               {
                   const string appId = "MassTransitMultibus.Tests";

                   // For validating messages received
                   services.AddSingleton<Validator<ICustomConsumerIdBus>>();
                   services.AddSingleton<Validator<IGenericTypesBus>>();
                   services.AddSingleton<Validator<IOnlyMyMessagesBus>>();
                   services.AddSingleton<Validator<ISimplifiedRegistrationBus>>();

                   // NOTE: can use same or different "physical" bus for each group below as they will be logically separate in MassTransit

                   // For testing custom consumer IDs
                   services
                       .RegisterMessageConsumer(
                           typeof(AllMessagesConsumerDefinition<ICustomConsumerIdBus, AllMessagesDefaultConsumerIdConsumer, AllMessagesDefaultConsumerIdMessage>))
                       .RegisterMessageConsumer(
                           typeof(AllMessagesConsumerDefinition<ICustomConsumerIdBus, AllMessagesCustomConsumerIdConsumer, AllMessagesCustomConsumerIdMessage>),
                           AllMessagesCustomConsumerId)
                       .RegisterMessageConsumer(
                           typeof(OnlyMyMessagesConsumerDefinition<ICustomConsumerIdBus, OnlyMyMessagesDefaultConsumerIdConsumer, OnlyMyMessagesDefaultConsumerIdMessage>))
                       .RegisterMessageConsumer(
                               typeof(OnlyMyMessagesConsumerDefinition<ICustomConsumerIdBus, OnlyMyMessagesCustomConsumerIdConsumer, OnlyMyMessagesCustomConsumerIdMessage>),
                               OnlyMyMessagesCustomConsumerId)
                       .AddMessaging(
                           hostingContext.Configuration.GetSection("TestMessageBus")?.Get<MessagingConfiguration<ICustomConsumerIdBus>>(),
                           appId,
                           new InMemoryTransport()); // for testing, default to InMemory

                   // For testing support of messages with generic parameters
                   services
                           .RegisterMessageConsumer(
                               typeof(AllMessagesConsumerDefinition<IGenericTypesBus,
                                   GenericTypesConsumer<GenericTypesMessage>,
                                   GenericTypesMessage>))
                           .RegisterMessageConsumer(
                                   typeof(AllMessagesConsumerDefinition<IGenericTypesBus,
                                       GenericTypesConsumer<GenericTypesMessage<Thing1A>>,
                                       GenericTypesMessage<Thing1A>>))
                           .RegisterMessageConsumer(
                                   typeof(AllMessagesConsumerDefinition<IGenericTypesBus,
                                       GenericTypesConsumer<GenericTypesMessage<IGeneric1>>,
                                       GenericTypesMessage<IGeneric1>>))
                           .RegisterMessageConsumer(
                                   typeof(AllMessagesConsumerDefinition<IGenericTypesBus,
                                       GenericTypesConsumer<GenericTypesMessage<Thing1A, Generic2X>>,
                                       GenericTypesMessage<Thing1A, Generic2X>>))
                           .RegisterMessageConsumer(
                                   typeof(AllMessagesConsumerDefinition<IGenericTypesBus,
                                       GenericTypesConsumer<GenericTypesMessage<IGeneric1, IGeneric2>>,
                                       GenericTypesMessage<IGeneric1, IGeneric2>>))
                           .AddMessaging(
                               hostingContext.Configuration.GetSection("TestMessageBus")?.Get<MessagingConfiguration<IGenericTypesBus>>(),
                               appId,
                               new InMemoryTransport()); // for testing, default to InMemory

                   // For testing filtering "only my messages"
                   services
                       .RegisterMessageConsumer(
                           typeof(AllMessagesConsumerDefinition<IOnlyMyMessagesBus, AllMessagesConsumer, AllMessagesMessage>))
                       .RegisterMessageConsumer(
                           typeof(OnlyMyMessagesConsumerDefinition<IOnlyMyMessagesBus, OnlyMyMessagesConsumer, OnlyMyMessagesMessage>))
                       .AddMessaging(
                           hostingContext.Configuration.GetSection("TestMessageBus")?.Get<MessagingConfiguration<IOnlyMyMessagesBus>>(),
                           appId,
                           new InMemoryTransport()); // for testing, default to InMemory

                   // For testing simplified consumer registration
                   services
                       .RegisterMessageConsumer(
                           typeof(AllMessagesConsumerDefinition<ISimplifiedRegistrationBus, SimplifiedRegistrationConsumer, SimplifiedRegistrationMessage>))
                       .RegisterMessageConsumer( // for testing can add additional generic type arguments to ConsumerDefinition
                           typeof(MoreGenericsConsumerDefinition<ISimplifiedRegistrationBus, MoreGenericsConsumer, MoreGenericsMessage, Something>))
                       .RegisterMessageConsumer( // for testing consumers and and messages can have generic type arguments
                           typeof(MoreGenericsConsumerDefinition<ISimplifiedRegistrationBus,
                               MoreGenericsConsumer<MoreGenericsMessage<IThing1, IThing2>>,
                               MoreGenericsMessage<IThing1, IThing2>,
                               Something>))
                       .AddMessaging(
                           hostingContext.Configuration.GetSection("TestMessageBus")?.Get<MessagingConfiguration<ISimplifiedRegistrationBus>>(),
                           appId,
                           new InMemoryTransport()); // for testing, default to InMemory

                   // handles starting / stopping bus(es)
                   services.AddMessagingHostedService();
               });
    }
}