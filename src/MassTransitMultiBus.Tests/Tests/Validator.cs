﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using MassTransit;
using MassTransitMultiBus.Messaging.Configurations;

namespace MassTransitMultiBus.Tests.Tests
{
    public class Validator<TBus>
        where TBus : IBus
    {
        public int MillisecondsToWait = 10000;
        public IList<Exception> ConsumerContextException { get; } = new List<Exception>();

        private readonly TBus _testBus;
        private readonly IMessagingConfiguration<TBus> _busConfiguration;
        private readonly IDictionary<string, Expected> _expecteds = new Dictionary<string, Expected>();
        private readonly IList<Task> _publishTasks = new List<Task>();
        private readonly AutoResetEvent _autoResetEvent = new AutoResetEvent(false);
        private readonly Mutex _mutex = new Mutex();

        private string ComposeKey(Expected expected) => ComposeKey(expected.Message.CorrelationId, expected.ConsumerName);
        private string ComposeKey(string correlationId, string consumerName) => $"{correlationId}::{consumerName}";

        public Validator(TBus testBus, IMessagingConfiguration<TBus> busConfiguration)
        {
            _testBus = testBus;
            _busConfiguration = busConfiguration;
        }

        public void InitializeValidator()
        {
            _expecteds.Clear();
            _publishTasks.Clear();
            _autoResetEvent.Reset();
            ConsumerContextException.Clear();
        }
        public Expected PrepareMessage<TMessage>(
            string consumerName,
            string customConsumerId = null,
            string alternateAppId = null,
            bool shouldNotReceive = false)
            where TMessage : class, ITestDataMessage
        {
            var message = Activator.CreateInstance<TMessage>();

            message.CorrelationId = Guid.NewGuid().ToString("N");
            message.MessageRequestorId = alternateAppId ?? _busConfiguration.AppId;
            message.Data = $"{typeof(TMessage)}-{message.CorrelationId}";

            var expected = new Expected
            {
                Message = message,
                ConsumerName = consumerName,
                CustomConsumerId = customConsumerId,
                ShouldNotReceive = shouldNotReceive
            };
            _expecteds.Add(ComposeKey(expected), expected);

            _publishTasks.Add(new Task(() => _testBus.Publish((object)message)));

            return expected;
        }

        /// <summary>
        /// Use this method to send a concrete message to a consumer with interface types
        /// </summary>
        /// <returns></returns>
        public Expected PrepareMessage<TMessage, TIMessage>(
            string consumerName,
            string customConsumerId = null,
            string alternateAppId = null,
            bool shouldNotReceive = false)
            where TMessage : class, ITestDataMessage
            where TIMessage : class
        {
            var message = Activator.CreateInstance<TMessage>();

            message.CorrelationId = Guid.NewGuid().ToString("N");
            message.MessageRequestorId = alternateAppId ?? _busConfiguration.AppId;
            message.Data = $"{typeof(TMessage)}-{message.CorrelationId}";

            var expected = new Expected
            {
                Message = message,
                ConsumerName = consumerName,
                CustomConsumerId = customConsumerId,
                ShouldNotReceive = shouldNotReceive
            };
            _expecteds.Add(ComposeKey(expected), expected);

            _publishTasks.Add(new Task(() => _testBus.Publish<TIMessage>(message)));

            return expected;
        }

        public async Task ActAndAssert()
        {
            // Act - Publish test messages
            foreach (var task in _publishTasks) { task.Start(); }
            await Task.WhenAll(_publishTasks.ToArray());

            // Wait for messages
            var sw = new Stopwatch();
            sw.Start();

            // save result of waiting
            var finishedBeforeTimeout = _autoResetEvent.WaitOne(MillisecondsToWait);

            sw.Stop();
            var receivedCount = _expecteds.Count(x => x.Value.Received);
            var expectedReceivedCount = _expecteds.Count(x => !x.Value.ShouldNotReceive);

            // Assert
            ConsumerContextException.Should().BeEmpty();
            receivedCount.Should().Be(expectedReceivedCount);
            finishedBeforeTimeout.Should().BeTrue();
        }

        public void ValidateMessage<TMessage>(
            string headerCorrelationId, TMessage message, string consumerName, string endpointName)
            where TMessage : class, ITestDataMessage
        {
            try
            {
                // Ignore unexpected messages from previous failed tests
                var key = ComposeKey(headerCorrelationId, consumerName);
                var match = _expecteds.TryGetValue(key, out var expected);
                if (!match)
                {
                    return;
                }

                // Ensure setting Received and checking for count of Received messages is thread safe 
                _mutex.WaitOne(MillisecondsToWait);

                if (_autoResetEvent.WaitOne(0))
                {
                    // ignore if consumer received after test timed out
                    return;
                }

                expected.ShouldNotReceive.Should().BeFalse("If true, received a message that should not have received");
                expected.Received.Should().BeFalse("If true, received this message again");
                expected.Received = true; // set to true so can be checked 
                message.Should().BeEquivalentTo(expected.Message);
                consumerName.Should().Be(expected.ConsumerName);
                endpointName.Should().NotBeNullOrEmpty();
                if (expected.CustomConsumerId != null)
                {
                    // Check that endpointName contains the custom consumerId
                    endpointName.Should().Contain(expected.CustomConsumerId);
                }

                // If have received all expected, set AutoResetEvent so test will continue
                if (_expecteds.All(x => x.Value.Received || x.Value.ShouldNotReceive))
                {
                    // have received all messages
                    _autoResetEvent.Set();
                }
            }
            catch (Exception e)
            {
                // Validating the message is done within a consume context so exceptions
                // cannot be caught by test thread, so save for checking within test thread
                ConsumerContextException.Add(e);
                _autoResetEvent.Set();
            }
            finally
            {
                _mutex.ReleaseMutex();
            }
        }
    }
}