using System.Threading.Tasks;
using MassTransitMultiBus.Tests.CustomConsumerId;
using Xunit;

namespace MassTransitMultiBus.Tests.Tests
{
    public class CustomConsumerIdTests
    {
        private readonly Validator<ICustomConsumerIdBus> _validator;

        public CustomConsumerIdTests(Validator<ICustomConsumerIdBus> validator)
        {
            _validator = validator;
        }

        [Fact]
        public async Task CanConsumeAllMessagesDefaultConsumerIdMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<AllMessagesDefaultConsumerIdMessage>(
                typeof(AllMessagesDefaultConsumerIdConsumer).FullName);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeAllMessagesCustomConsumerIdMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<AllMessagesCustomConsumerIdMessage>(
                typeof(AllMessagesCustomConsumerIdConsumer).FullName,
                Startup.AllMessagesCustomConsumerId);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeOnlyMyMessagesDefaultConsumerIdMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<OnlyMyMessagesDefaultConsumerIdMessage>(
                typeof(OnlyMyMessagesDefaultConsumerIdConsumer).FullName);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeOnlyMyMessagesCustomConsumerIdMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<OnlyMyMessagesCustomConsumerIdMessage>(
                typeof(OnlyMyMessagesCustomConsumerIdConsumer).FullName,
                Startup.OnlyMyMessagesCustomConsumerId);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeMultipleOfEachTypeOfTestMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            for (var i = 0; i < 10; i++)
            {
                _validator.PrepareMessage<AllMessagesDefaultConsumerIdMessage>(
                    typeof(AllMessagesDefaultConsumerIdConsumer).FullName);
                _validator.PrepareMessage<AllMessagesCustomConsumerIdMessage>(
                    typeof(AllMessagesCustomConsumerIdConsumer).FullName,
                    Startup.AllMessagesCustomConsumerId);
                _validator.PrepareMessage<OnlyMyMessagesDefaultConsumerIdMessage>(
                    typeof(OnlyMyMessagesDefaultConsumerIdConsumer).FullName);
                _validator.PrepareMessage<OnlyMyMessagesCustomConsumerIdMessage>(
                    typeof(OnlyMyMessagesCustomConsumerIdConsumer).FullName,
                    Startup.OnlyMyMessagesCustomConsumerId);
            }

            await _validator.ActAndAssert();
        }
    }
}
