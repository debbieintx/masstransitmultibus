﻿using MassTransitMultiBus.Tests.SimplifiedRegistration;
using Xunit;

namespace MassTransitMultiBus.Tests.Tests
{
    public class SimplifiedRegistrationTests
    {
        private readonly Validator<ISimplifiedRegistrationBus> _validator;

        public SimplifiedRegistrationTests(Validator<ISimplifiedRegistrationBus> validator)
        {
            _validator = validator;
        }

        [Fact]
        public async void CanRegisterUsingSimplerMethod()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<SimplifiedRegistrationMessage>(
                typeof(SimplifiedRegistrationConsumer).FullName);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async void CanRegisterDefinitionWithExtraGenericParameter()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<MoreGenericsMessage>(
                typeof(MoreGenericsConsumer).FullName);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async void CanRegisterEvenMoreGenericsConsumer()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<MoreGenericsMessage<Thing1A, Thing2X>, MoreGenericsMessage<IThing1, IThing2>>(
                typeof(MoreGenericsConsumer<MoreGenericsMessage<IThing1, IThing2>>).FullName);

            await _validator.ActAndAssert();
        }
    }
}