using System;
using FluentAssertions;
using MassTransitMultiBus.Messaging.Extensions;
using Xunit;

namespace MassTransitMultiBus.Tests.Tests
{
    public class BasicTests
    {
        [Theory]
        [InlineData("a", "a")]
        [InlineData("A", "A")]
        [InlineData("0", "0")]
        [InlineData("0-~._/9", "0-~._/9")]
        [InlineData("0`[]-~._\\$/9", "0-~._/9")]
        [InlineData("AAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBB",
            "AAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBB")]
        public void EndpointNamesGetSanitizedAndValidated(string initial, string expected)
        {
            var sanitized = initial.SanitizeEndpointName();
            sanitized.Should().Be(expected);
        }

        [Theory]
        [InlineData("-")]
        [InlineData("ab-")]
        [InlineData("-bc")]
        [InlineData("xAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBBAAAAAAAAAABBBBBBBBBB")]
        public void InvalidEndpointNamesGetRejected(string initial)
        {
            var ex = Assert.Throws<Exception>(initial.SanitizeEndpointName);
            ex.Message.Should().Contain("match the expression");
        }
    }
}
