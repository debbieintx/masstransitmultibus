using System.Threading.Tasks;
using MassTransitMultiBus.Tests.GenericTypes;
using MassTransitMultiBus.Tests.SimplifiedRegistration;
using Xunit;

namespace MassTransitMultiBus.Tests.Tests
{
    public class GenericTypesTests
    {
        private readonly Validator<IGenericTypesBus> _validator;

        public GenericTypesTests(Validator<IGenericTypesBus> validator)
        {
            _validator = validator;
        }

        [Fact]
        public async Task CanConsumeTMessageMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<GenericTypesMessage>(
                typeof(GenericTypesConsumer<GenericTypesMessage>).FullName);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeTMessageGeneric1AMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<GenericTypesMessage<Thing1A>>(
                typeof(GenericTypesConsumer<GenericTypesMessage<Thing1A>>).FullName);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeTMessageIGeneric1Message()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<GenericTypesMessage<IGeneric1>>(
                typeof(GenericTypesConsumer<GenericTypesMessage<IGeneric1>>).FullName);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeTMessageGeneric1AGeneric2XMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<GenericTypesMessage<Thing1A, Generic2X>>(
                typeof(GenericTypesConsumer<GenericTypesMessage<Thing1A, Generic2X>>).FullName);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeTMessageIGeneric1IGeneric2Message()
        {
            // Arrange
            _validator.InitializeValidator();

            _validator.PrepareMessage<GenericTypesMessage<IGeneric1, IGeneric2>>(
                typeof(GenericTypesConsumer<GenericTypesMessage<IGeneric1, IGeneric2>>).FullName);

            await _validator.ActAndAssert();
        }

        [Fact]
        public async Task CanConsumeMultipleOfEachTypeOfMessage()
        {
            // Arrange
            _validator.InitializeValidator();

            for (var i = 0; i < 10; i++)
            {
                _validator.PrepareMessage<GenericTypesMessage>(
                    typeof(GenericTypesConsumer<GenericTypesMessage>).FullName);
                _validator.PrepareMessage<GenericTypesMessage<Thing1A>>(
                    typeof(GenericTypesConsumer<GenericTypesMessage<Thing1A>>).FullName);
                _validator.PrepareMessage<GenericTypesMessage<IGeneric1>>(
                    typeof(GenericTypesConsumer<GenericTypesMessage<IGeneric1>>).FullName);
                _validator.PrepareMessage<GenericTypesMessage<Thing1A, Generic2X>>(
                    typeof(GenericTypesConsumer<GenericTypesMessage<Thing1A, Generic2X>>).FullName);
                _validator.PrepareMessage<GenericTypesMessage<IGeneric1, IGeneric2>>(
                    typeof(GenericTypesConsumer<GenericTypesMessage<IGeneric1, IGeneric2>>).FullName);
            }

            await _validator.ActAndAssert();
        }
    }
}
