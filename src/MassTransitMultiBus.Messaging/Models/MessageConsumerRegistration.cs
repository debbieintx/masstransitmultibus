﻿using System;
using System.Collections.Generic;
using System.Linq;
using MassTransit;

namespace MassTransitMultiBus.Messaging.Models
{
    // Used to register consumers to be configured with Mass Transit via this library
    public class MessageConsumerRegistration<TBus> : IMessageConsumerRegistration<TBus>
        where TBus : IBus
    {
        public MessageConsumerRegistration(
            Type consumerDefinitionType,
            Type consumerType,
            string consumerId = null)
        {
            ConsumerDefinitionType = consumerDefinitionType;
            ConsumerType = consumerType;
            ConsumerId = string.IsNullOrWhiteSpace(consumerId)
                ? GenerateConsumerId(consumerDefinitionType)
                : consumerId;
        }

        public Type ConsumerDefinitionType { get; }
        public Type ConsumerType { get; }
        public string ConsumerId { get; }

        /// <summary>
        /// For use by exceptions / trouble-shooting
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"{nameof(ConsumerType)}: \"{ConsumerType.FullName}\"" +
                   $"{Environment.NewLine}{nameof(ConsumerDefinitionType)}: \"{ConsumerDefinitionType}\"" +
                   $"{Environment.NewLine}{nameof(ConsumerId)}: \"{ConsumerId}\"";
        }

        private static string GenerateConsumerId(Type consumerDefinitionType)
        {
            // Expect ConsumerDefinitionType to have generic parameters TBus, TConsumer, TMessage.
            // Construction default ConsumerId as TMessage:TConsumer:ConsumerDefinitionType:TBus.
            // Include generic parameters for TMessage
            var generics = consumerDefinitionType.GenericTypeArguments;
            var parts = new List<string>();
            if (generics.Length > 2)
            {
                // TMessage
                var messageParts = new List<string> { generics[2].Name };
                messageParts.AddRange(generics[2].GenericTypeArguments.Select(x => x.Name));
                parts.Add(string.Join(".", messageParts));
            }
            if (generics.Length > 1) parts.Add(generics[1].Name); // TConsumer
            parts.Add(consumerDefinitionType.Name);
            if (generics.Length > 0) parts.Add(generics[0].Name); // TBus
            return string.Join("-", parts);
        }
    }
}