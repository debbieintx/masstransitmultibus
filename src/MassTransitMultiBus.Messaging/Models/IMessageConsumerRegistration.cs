﻿using System;
using MassTransit;

namespace MassTransitMultiBus.Messaging.Models
{
    /// <summary>
    /// Use to add Mass Transit Consumers and to inject configuration information such as
    /// ConsumerId into the ConsumerDefinition.
    /// </summary>
    /// <typeparam name="TBus"></typeparam>
    public interface IMessageConsumerRegistration<TBus> : IMessageConsumerRegistrationInformation
        where TBus : IBus
    {
    }

    /// <summary>
    /// Use to add Mass Transit Consumers and to inject configuration information such as
    /// ConsumerId into the ConsumerDefinition.
    /// </summary>
    public interface IMessageConsumerRegistrationInformation
    {
        Type ConsumerDefinitionType { get; }
        Type ConsumerType { get; }
        string ConsumerId { get; }

        /// <summary>
        /// For use by exceptions / trouble-shooting
        /// </summary>
        /// <returns></returns>
        string ToString();
    }
}