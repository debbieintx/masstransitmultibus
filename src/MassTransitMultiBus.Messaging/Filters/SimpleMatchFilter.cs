﻿using System;
using System.Threading.Tasks;
using GreenPipes;
using MassTransit;
using ConsumeContext = MassTransit.ConsumeContext;

namespace MassTransitMultiBus.Messaging.Filters
{
    public class SimpleMatchFilter<TMessage, TValue> : IFilter<ConsumeContext>
        where TMessage : class
        where TValue : class, IComparable
    {
        private readonly string _key;
        private readonly TValue _match;

        public SimpleMatchFilter(string key, TValue match)
        {
            if (string.IsNullOrEmpty(key))
            {
                throw new ArgumentOutOfRangeException(nameof(key));
            }
            _key = key;
            _match = match ?? throw new ArgumentNullException(nameof(match));
        }

        public async Task Send(ConsumeContext context, IPipe<ConsumeContext> next)
        {
            if (context.Headers.TryGetHeader(_key, out var getResult))
            {
                var value = getResult as TValue;
                if (_match.Equals(value))
                {
                    // Message matches so send it to next pipe
                    await next.Send(context);
                    return;
                }
            }

            // Message does not match, so consume it so that it doesn't end up in the dead letter queue
            await context.NotifyConsumed(context as ConsumeContext<TMessage>, TimeSpan.Zero, nameof(SimpleMatchFilter<TMessage, TValue>));
        }

        public void Probe(ProbeContext context)
        {
            context.CreateFilterScope(nameof(SimpleMatchFilter<TMessage, TValue>));
        }
    }
}