﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using GreenPipes;
using GreenPipes.Internals.Extensions;
using GreenPipes.Specifications;
using MassTransit;
using MassTransit.Definition;
using MassTransit.MultiBus;
using MassTransitMultiBus.Abstractions.Interfaces;
using MassTransitMultiBus.Abstractions.Models;
using MassTransitMultiBus.Messaging.Configurations;
using MassTransitMultiBus.Messaging.Models;
using Microsoft.Extensions.DependencyInjection;

namespace MassTransitMultiBus.Messaging.Extensions
{
    public static class MessagingExtensions
    {
        public static IServiceCollection RegisterMessageConsumer(
            this IServiceCollection services, Type consumerDefinitionType, string consumerId = null)
        {
            // Verify consumerDefinitionType meets the basic constraints
            if (consumerDefinitionType == null)
            {
                throw new ArgumentNullException(nameof(consumerDefinitionType));
            }
            if (!consumerDefinitionType.IsClass)
            {
                throw new ArgumentException($"{nameof(consumerDefinitionType)} must be a class");
            }
            if (consumerDefinitionType.GetInterface(nameof(IConsumerDefinition)) == null)
            {
                throw new ArgumentException($"{nameof(consumerDefinitionType)} must implement {nameof(IConsumerDefinition)}");
            }

            // Verify the minimum generic type arguments are present
            var busType = consumerDefinitionType.GenericTypeArguments.ElementAtOrDefault(0);
            var consumerType = consumerDefinitionType.GenericTypeArguments.ElementAtOrDefault(1);
            var messageType = consumerDefinitionType.GenericTypeArguments.ElementAtOrDefault(2);
            if (busType == null || consumerType == null || messageType == null)
            {
                throw new ArgumentException($"{nameof(consumerDefinitionType)} expecting TBus, TConsumer," +
                                            " and TMessage as the first three generic type arguments but found" +
                                            $" {nameof(consumerType.GenericTypeArguments.Length)} argument(s)");
            }

            // Verify TBus is an IBus
            if (!busType.HasInterface(typeof(IBus)))
            {
                throw new ArgumentException($"{nameof(consumerDefinitionType)} first generic type argument must implement {nameof(IBus)}");
            }

            // Validate TConsumer is a class which implements IConsumer<TMessage>
            if (!consumerType.IsClass)
            {
                throw new ArgumentException($"{nameof(consumerDefinitionType)} TConsumer must be a class");
            }
            var consumerMustImplement = typeof(IConsumer<>).MakeGenericType(messageType);
            if (!consumerType.HasInterface(consumerMustImplement))
            {
                throw new ArgumentException($"{nameof(consumerDefinitionType)} TConsumer must implement {consumerMustImplement}");
            }

            // Make types for registration
            var iRegistrationType = typeof(IMessageConsumerRegistration<>).MakeGenericType(busType);
            var registrationType = typeof(MessageConsumerRegistration<>).MakeGenericType(busType);

            // Get existing registrations to check for duplicates
            var existingRegistrations = services
                .Where(x => x.ServiceType == iRegistrationType)
                .Select(x => (IMessageConsumerRegistrationInformation)x.ImplementationInstance)
                .ToList();

            // Reject if consumer definition type is already registered (don't need to check 
            // consumer type since it was extracted from definition type)
            if (existingRegistrations
                .Any(x => x.ConsumerDefinitionType == consumerDefinitionType))
            {
                throw new Exception($"{iRegistrationType.Name} {busType.Name} already has registration with" +
                                    $" {nameof(IMessageConsumerRegistrationInformation.ConsumerDefinitionType)} {consumerDefinitionType}");
            }

            // Reject if consumer ID is already registered. The default value is generated so
            // first need to create the registration instance.
            var registration = (IMessageConsumerRegistrationInformation)Activator.CreateInstance(registrationType,
                consumerDefinitionType, consumerType, consumerId);
            if (existingRegistrations
                .Any(x => x.ConsumerId == registration.ConsumerId))
            {
                throw new Exception($"{iRegistrationType.Name} {busType.Name} already has registration with" +
                                    $" {nameof(IMessageConsumerRegistrationInformation.ConsumerId)} {consumerDefinitionType}");
            }

            // Add registration
            services.AddSingleton(iRegistrationType,
                Activator.CreateInstance(registrationType,
                consumerDefinitionType, consumerType, consumerId));

            return services;
        }

        public static IServiceCollection AddMessaging<TBus>(
            this IServiceCollection services,
            IMessagingConfiguration<TBus> messageBusConfiguration,
            string overrideAppId = null,
            IMessageTransport defaultMessageTransport = null)
            where TBus : class, IBus
        {
            // Basic validation / initialization of configuration
            if (messageBusConfiguration == null)
            {
                throw new ArgumentNullException(nameof(messageBusConfiguration));
            }
            if (overrideAppId != null)
            {
                messageBusConfiguration.AppId = overrideAppId;
            }
            if (string.IsNullOrWhiteSpace(messageBusConfiguration.AppId))
            {
                throw new ArgumentException("Must not be null or white space", nameof(messageBusConfiguration.AppId));
            }

            // use defaultMessageTransport if MessageTransport is null; if still null, throw exception
            messageBusConfiguration.MessageTransport ??= defaultMessageTransport
                ?? throw new ArgumentNullException(nameof(messageBusConfiguration.MessageTransport));

            // Build list of exception types to retry in consumers
            var invalidTypeNames = new List<string>();
            foreach (var typeName in messageBusConfiguration.ExceptionsToRetryAssemblyQualifiedNames)
            {
                Type type = null;
                try
                {
                    type = Type.GetType(typeName);
                }
                catch (Exception)
                {
                    /* ignore */
                }

                if (type != null && typeof(Exception).IsAssignableFrom(type))
                {
                    messageBusConfiguration.ExceptionTypesToRetry.Add(type);
                }
                else
                {
                    invalidTypeNames.Add($"\"{typeName}\"");
                }
            }
            if (invalidTypeNames.Any())
            {
                throw new Exception(
                    $"All names in {nameof(messageBusConfiguration.ExceptionsToRetryAssemblyQualifiedNames)} must be assignable from {typeof(Exception)}." +
                    $" The following invalid name(s) were found: {string.Join("; ", invalidTypeNames)}");
            }

            // Add message bus configuration to services
            var existingMessageBusConfiguration = services?
                .Where(x => x.ServiceType == typeof(IMessagingConfiguration<TBus>))
                .FirstOrDefault();
            if (existingMessageBusConfiguration != null)
            {
                // Last one wins
                services.Remove(existingMessageBusConfiguration);
            }
            services.AddSingleton(messageBusConfiguration);

            // Add MassTransit
            services.AddMassTransit<TBus>(busConfigurator =>
            {
                // Get message consumer registrations 
                var registrations = services?.Where(
                    x => x.ServiceType == typeof(IMessageConsumerRegistration<TBus>))
                    .Select(x => (IMessageConsumerRegistration<TBus>)x.ImplementationInstance)
                    .ToList() ?? new List<IMessageConsumerRegistration<TBus>>();

                // Register consumers
                foreach (var registration in registrations)
                {
                    busConfigurator.AddConsumer(
                        registration.ConsumerType,
                        registration.ConsumerDefinitionType);
                }

                if (messageBusConfiguration.MessageTransport is InMemoryTransport)
                {
                    // WARNING The in-memory transport is intended for use within a single process only.
                    // It cannot be used to communicate between multiple processes (even if they are on
                    // the same machine).
                    busConfigurator.UsingInMemory((busRegistrationContext, inMemoryBusFactoryConfigurator) =>
                    {
                        // todo add in memory scheduling?
                        inMemoryBusFactoryConfigurator.ConfigureEndpoints(busRegistrationContext);

                        // Common configuration
                        inMemoryBusFactoryConfigurator.ConfigureBusFactory(busRegistrationContext);
                    });
                }
                else if (messageBusConfiguration.MessageTransport is AzureServiceBusTransport
                    azureServiceBusConfiguration)
                {
                    busConfigurator.UsingAzureServiceBus((busRegistrationContext, serviceBusBusFactoryConfigurator) =>
                    {
                        // AzureServiceBus-specific configuration
                        serviceBusBusFactoryConfigurator.Host(azureServiceBusConfiguration.ConnectionStrings);
                        busConfigurator.AddServiceBusMessageScheduler();
                        serviceBusBusFactoryConfigurator.UseServiceBusMessageScheduler();
                        serviceBusBusFactoryConfigurator.ConfigureEndpoints(busRegistrationContext);

                        // Common configuration
                        serviceBusBusFactoryConfigurator.ConfigureBusFactory(busRegistrationContext);
                    });
                }
                else if (messageBusConfiguration.MessageTransport is RabbitMQTransport rabbitMQConfiguration)
                {
                    busConfigurator.UsingRabbitMq((busRegistrationContext, rabbitMqBusFactoryConfigurator) =>
                    {
                        // RabbitMQ-specific configuration
                        rabbitMqBusFactoryConfigurator.Host(
                            new Uri($"{rabbitMQConfiguration.ConnectionStrings}{rabbitMQConfiguration.VirtualHost}"),
                            h =>
                            {
                                h.Username(rabbitMQConfiguration.UserName);
                                h.Password(rabbitMQConfiguration.Password);
                            });
                        busConfigurator.AddRabbitMqMessageScheduler();
                        rabbitMqBusFactoryConfigurator.UseRabbitMqMessageScheduler();
                        rabbitMqBusFactoryConfigurator.ConfigureEndpoints(busRegistrationContext);

                        // Common configuration
                        rabbitMqBusFactoryConfigurator.ConfigureBusFactory(busRegistrationContext);
                    });
                }
                else
                {
                    throw new Exception(
                        $"Unsupported transport type for message bus: {messageBusConfiguration.MessageTransport.GetType()}");
                }
            });

            return services;
        }

        /// <summary>
        /// Simply calls AddMassTransitHostedService but keeps library users from having to include
        /// MassTransit libraries if they are not using it directly
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddMessagingHostedService(this IServiceCollection services)
        {
            return services.AddMassTransitHostedService();
        }

        public static void ConfigureBusFactory(
           this IBusFactoryConfigurator busFactoryConfigurator,
           IBusRegistrationContext busRegistrationContext)
        {
            // Add meta data for all IMessageBase messages that are published
            busFactoryConfigurator.UsePublishMetaDataPipe<IMessageBase>();

            // limit concurrency; todo make configurable
            busFactoryConfigurator.UseConcurrencyLimit(3);

            // health checks
            busFactoryConfigurator.UseHealthCheck(busRegistrationContext);
        }

        public static void UsePublishMetaDataPipe<TMessage>(this IBusFactoryConfigurator cfg)
            where TMessage : class, IMessageBase
        {
            // Add meta data to header
            cfg.ConfigurePublish(publishPipeConfigurator => publishPipeConfigurator.AddPipeSpecification(
                new DelegatePipeSpecification<PublishContext<TMessage>>(publishContext =>
                {
                    publishContext.Headers.Set(nameof(IMessageBase.MessageRequestorId),
                        publishContext.Message.MessageRequestorId);
                    if (Guid.TryParse(publishContext.Message.CorrelationId, out var correlationId))
                    {
                        publishContext.CorrelationId = correlationId;
                    }
                })));
        }

        /// <summary>
        /// For Endpoint name "sanitizing" and validation rules, could be transport-specific and injected
        /// but for simplicity this uses Azure rules, the most restrictive of Azure, RabbitMQ, and InMemory.
        /// The original reason for creating a sanitizing function is to be able to use type names
        /// of generic types (which contain characters that are not valid in endpoint names).
        /// </summary>
        public static string SanitizeEndpointName(this string endpointName)
        {
            if (endpointName == null) throw new ArgumentNullException(nameof(endpointName));

            // Remove invalid characters
            var sanitized = MatchInvalidCharacters
                .Replace(endpointName, string.Empty);

            // Validate
            if (!ValidFormat.IsMatch(sanitized) || sanitized.Length < 1 || sanitized.Length > MaxLength)
            {
                throw new Exception($"Endpoint name {sanitized} is not valid. It must be between {MinLength} and {MaxLength} characters" +
                                    $" and match the expression {MustMatch}");
            }

            return sanitized;
        }

        // This matches any character that is not allowed so that it can be removed
        private static readonly Regex MatchInvalidCharacters = new Regex("[^\\w-\\.\\/\\~]");

        // This is the regular expression pattern that is used to validate new names in Azure Portal
        private const string MustMatch = "^[A-Za-z0-9]$|^[A-Za-z0-9][\\w-\\.\\/\\~]*[A-Za-z0-9]$";

        private static readonly Regex ValidFormat = new Regex(MustMatch);
        private const int MinLength = 1;
        private const int MaxLength = 260;
    }
}