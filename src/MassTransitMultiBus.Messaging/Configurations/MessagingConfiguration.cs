﻿using System;
using System.Collections.Generic;
using MassTransit;
using MassTransitMultiBus.Abstractions.Enums;
using MassTransitMultiBus.Abstractions.Interfaces;
using MassTransitMultiBus.Abstractions.Models;

namespace MassTransitMultiBus.Messaging.Configurations
{
    public class MessagingConfiguration<TBus> : IMessagingConfiguration<TBus>
        where TBus : IBus
    {
        public string AppId { get; set; }

        // if not set explicitly, will derive from MessageTransportType
        private IMessageTransport _messageTransport;
        public IMessageTransport MessageTransport
        {
            get =>
                _messageTransport ?? Transport switch
                {
                    MessageTransportType.InMemory => InMemory,
                    MessageTransportType.RabbitMQ => RabbitMQ,
                    MessageTransportType.AzureServiceBus => Azure,
                    _ => null
                };
            set => _messageTransport = value;
        }

        public IList<double> RetryIntervalsInMilliseconds { get; set; }
        public int RescheduleLimit { get; set; }
        public int RescheduleIntervalInSeconds { get; set; }
        public IList<string> ExceptionsToRetryAssemblyQualifiedNames { get; set; } = new List<string>();
        public IList<Type> ExceptionTypesToRetry { get; set; } = new List<Type>();
        public IList<string> RetryIfExceptionContains { get; set; } = new List<string>();

        // this should probably be named MessageTransportType but that would
        // require changes to a lot of configuration files
        public MessageTransportType Transport { get; set; }

        public InMemoryTransport InMemory { get; set; }
            = new InMemoryTransport();

        public AzureServiceBusTransport Azure { get; set; }
            = new AzureServiceBusTransport();

        public RabbitMQTransport RabbitMQ { get; set; }
            = new RabbitMQTransport();
    }
}
