﻿using System;
using System.Collections.Generic;
using MassTransit;
using MassTransitMultiBus.Abstractions.Interfaces;

namespace MassTransitMultiBus.Messaging.Configurations
{
    public interface IMessagingConfiguration<TBus>
        where TBus : IBus
    {
        string AppId { get; set; }
        IMessageTransport MessageTransport { get; set; }
        IList<double> RetryIntervalsInMilliseconds { get; set; }
        int RescheduleLimit { get; set; }
        int RescheduleIntervalInSeconds { get; set; }
        IList<string> ExceptionsToRetryAssemblyQualifiedNames { get; set; }
        IList<Type> ExceptionTypesToRetry { get; set; }
        IList<string> RetryIfExceptionContains { get; set; }
    }
}