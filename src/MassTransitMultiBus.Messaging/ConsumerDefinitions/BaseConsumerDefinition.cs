﻿using System;
using System.Collections.Generic;
using System.Linq;
using GreenPipes;
using GreenPipes.Configurators;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;
using MassTransitMultiBus.Abstractions.Interfaces;
using MassTransitMultiBus.Messaging.Configurations;
using MassTransitMultiBus.Messaging.Extensions;
using MassTransitMultiBus.Messaging.Filters;
using MassTransitMultiBus.Messaging.Models;

namespace MassTransitMultiBus.Messaging.ConsumerDefinitions
{
    /// <summary>
    /// Common configuration for message consumers. This is intended to be a useful consumer
    /// definition but not a complete solution for all situations. Other definitions may
    /// be needed for different use cases.
    /// </summary>
    /// <typeparam name="TBus"></typeparam>
    /// <typeparam name="TConsumer"></typeparam>
    /// <typeparam name="TMessage"></typeparam>
    public abstract class BaseConsumerDefinition<TBus, TConsumer, TMessage> : ConsumerDefinition<TConsumer>
        where TBus : IBus
        where TConsumer : class, IConsumer<TMessage>
        where TMessage : class
    {
        private readonly IMessagingConfiguration<TBus> _messageBusConfiguration;
        private readonly string _messageRequestorIdFilter;

        protected BaseConsumerDefinition(
            IMessagingConfiguration<TBus> messageBusConfiguration,
            IEnumerable<IMessageConsumerRegistration<TBus>> consumerRegistrations,
            Type consumerDefinitionType,
            string messageRequestorIdFilter = null)
        {
            _messageBusConfiguration = messageBusConfiguration;
            _messageRequestorIdFilter = messageRequestorIdFilter;

            // Get ConsumerId to set EndpointName, *which needs to be set in the constructor*.
            // This is an an awkward way to get this information, but there weren't any
            // MassTransit AddConsumer implementations that provided a way to add
            // additional parameters for the ConsumerDefinition when it is sent as a parameter.
            // Fortunately, this is only executed on startup.
            IMessageConsumerRegistration<TBus> consumerInformation;
            try
            {
                // duplicates should have already been caught, but just in case, use single
                consumerInformation = consumerRegistrations
                    .Single(x =>
                       x.ConsumerDefinitionType == consumerDefinitionType);
                if (string.IsNullOrWhiteSpace(consumerInformation.ConsumerId))
                {
                    throw new Exception($"Must not be null or white space {nameof(MessageConsumerRegistration<TBus>.ConsumerId)}");
                }
            }
            catch (Exception e)
            {
                throw new Exception($"Error getting {nameof(MessageConsumerRegistration<TBus>.ConsumerId)} in \"{consumerRegistrations.GetType()}\" for:" +
                                    $"{Environment.NewLine}{nameof(MessageConsumerRegistration<TBus>.ConsumerType)}: \"{typeof(TConsumer)}\"" +
                                    $"{Environment.NewLine}{nameof(MessageConsumerRegistration<TBus>.ConsumerDefinitionType)}: \"{consumerDefinitionType}\"." +
                                    $"{Environment.NewLine}{Environment.NewLine}ConsumerRegistrations:{Environment.NewLine}{consumerRegistrations?.ToString() ?? "null"}",
                    e);
            }

            var endpointName = $"{_messageBusConfiguration.AppId}-{consumerInformation.ConsumerId}"
                    .SanitizeEndpointName();
            EndpointName = endpointName;
        }

        protected override void ConfigureConsumer(
            IReceiveEndpointConfigurator endpointConfigurator,
            IConsumerConfigurator<TConsumer> consumerConfigurator)
        {
            IRetryConfigurator CommonRetryConfigurator(IRetryConfigurator retryConfigurator)
            {
                // Mass Transit supports different exception configurations for retry vs reschedule,
                // but this configuration uses the same set for both
                if (_messageBusConfiguration.ExceptionTypesToRetry.Any())
                {
                    retryConfigurator.Handle(_messageBusConfiguration.ExceptionTypesToRetry.ToArray());
                }
                if (_messageBusConfiguration.RetryIfExceptionContains.Any())
                {
                    retryConfigurator.Handle<Exception>(x => _messageBusConfiguration
                        .RetryIfExceptionContains.Any(s => x?.InnerException?.ToString().Contains(s) == true));
                }
                return retryConfigurator;
            }

            // todo add to configuration
            endpointConfigurator.UseConcurrencyLimit(3);
            consumerConfigurator.UseConcurrencyLimit(3);

            // Configure consumer to only receive results for jobs requested by this service if the filter is set.
            if (!string.IsNullOrEmpty(_messageRequestorIdFilter))
            {
                endpointConfigurator.UseFilter(
                    new SimpleMatchFilter<TMessage, string>(nameof(IMessageBase.MessageRequestorId), _messageBusConfiguration.AppId));
            }

            // Schedule redelivery (AKA second-level retry) policy for non-transient conditions such as a server crash.
            // This must be configured before In-Memory Retry is configured.
            if (_messageBusConfiguration.RescheduleLimit > 0 &&
                _messageBusConfiguration?.RescheduleIntervalInSeconds > 0)
            {
                endpointConfigurator.UseScheduledRedelivery(r =>
                {
                    r.Interval(_messageBusConfiguration.RescheduleLimit,
                        TimeSpan.FromSeconds(_messageBusConfiguration.RescheduleIntervalInSeconds));
                    CommonRetryConfigurator(r);
                });
            }

            // In-memory Retry policy for transient conditions such as a database lock.
            if (_messageBusConfiguration?.RetryIntervalsInMilliseconds != null &&
                _messageBusConfiguration.RetryIntervalsInMilliseconds.Any())
            {
                endpointConfigurator.UseMessageRetry(r =>
                {
                    r.Intervals(
                        _messageBusConfiguration.RetryIntervalsInMilliseconds.Select(TimeSpan.FromMilliseconds)
                            .ToArray());
                    CommonRetryConfigurator(r);
                });
            }
        }
    }
}