﻿using System.Collections.Generic;
using MassTransit;
using MassTransitMultiBus.Messaging.Configurations;
using MassTransitMultiBus.Messaging.Models;

namespace MassTransitMultiBus.Messaging.ConsumerDefinitions
{
    public class OnlyMyMessagesConsumerDefinition<TBus, TConsumer, TMessage> : BaseConsumerDefinition<TBus, TConsumer, TMessage>
        where TBus : IBus
        where TConsumer : class, IConsumer<TMessage>
        where TMessage : class
    {
        // set messageRequestorId to AppId so base class will filter out messages where MessageRequestorId != AppId
        public OnlyMyMessagesConsumerDefinition(
            IMessagingConfiguration<TBus> messageBusConfiguration,
            IEnumerable<IMessageConsumerRegistration<TBus>> consumerRegistrations)
            : base(
                messageBusConfiguration,
                consumerRegistrations,
                typeof(OnlyMyMessagesConsumerDefinition<TBus, TConsumer, TMessage>),
                messageBusConfiguration.AppId)
        {
        }
    }
}