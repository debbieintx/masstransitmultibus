﻿using System.Collections.Generic;
using MassTransit;
using MassTransitMultiBus.Messaging.Configurations;
using MassTransitMultiBus.Messaging.Models;

namespace MassTransitMultiBus.Messaging.ConsumerDefinitions
{
    public class AllMessagesConsumerDefinition<TBus, TConsumer, TMessage> : BaseConsumerDefinition<TBus, TConsumer, TMessage>
        where TBus : IBus
        where TConsumer : class, IConsumer<TMessage>
        where TMessage : class
    {
        // Set messageRequestorFilter to null so base class will not filter messages
        public AllMessagesConsumerDefinition(
            IMessagingConfiguration<TBus> messageBusConfiguration,
            IEnumerable<IMessageConsumerRegistration<TBus>> consumerRegistrations)
            : base(
                messageBusConfiguration,
                consumerRegistrations,
                typeof(AllMessagesConsumerDefinition<TBus, TConsumer, TMessage>))
        {
        }
    }
}