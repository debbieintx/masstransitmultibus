﻿namespace MassTransitMultiBus.Abstractions.Interfaces
{
    public interface IMessageBase
    {
        string CorrelationId { get; set; }
        string MessageRequestorId { get; set; }
    }
}