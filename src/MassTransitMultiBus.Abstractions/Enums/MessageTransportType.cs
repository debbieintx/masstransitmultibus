﻿namespace MassTransitMultiBus.Abstractions.Enums
{
    public enum MessageTransportType
    {
        Unknown = 0,
        InMemory = 1,
        RabbitMQ = 2,
        AzureServiceBus = 3,
    }
}