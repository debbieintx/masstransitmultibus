﻿using MassTransitMultiBus.Abstractions.Interfaces;

namespace MassTransitMultiBus.Abstractions.Models
{
    public class RabbitMQTransport : IMessageTransport
    {
        public string ConnectionStrings { get; set; }
        public string VirtualHost { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}