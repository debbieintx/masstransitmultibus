﻿using MassTransitMultiBus.Abstractions.Interfaces;

namespace MassTransitMultiBus.Abstractions.Models
{
    public class AzureServiceBusTransport : IMessageTransport
    {
        public string ConnectionStrings { get; set; }
    }
}